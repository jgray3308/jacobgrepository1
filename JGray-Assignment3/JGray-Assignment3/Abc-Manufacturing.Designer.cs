﻿namespace JGray_Assignment3
{
    partial class Abc_Manufacturing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.txtTransactNum = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dtmDateSelect = new System.Windows.Forms.DateTimePicker();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtSerialNumber = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtToolPurchased = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.txtDataDisplay = new System.Windows.Forms.RichTextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.rbOpenFile = new System.Windows.Forms.RadioButton();
            this.rbCreateFile = new System.Windows.Forms.RadioButton();
            this.btnCreateFile = new System.Windows.Forms.Button();
            this.btnRecordWrite = new System.Windows.Forms.Button();
            this.btnRecordDelete = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.txtTransactDelete = new System.Windows.Forms.TextBox();
            this.btnDisplayData = new System.Windows.Forms.Button();
            this.btnCloseFile = new System.Windows.Forms.Button();
            this.btnDeleteFile = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.txtMessageDisplay = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtFileName);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(328, 55);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Enter Filename:";
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(7, 20);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(315, 20);
            this.txtFileName.TabIndex = 0;
            // 
            // txtTransactNum
            // 
            this.txtTransactNum.Location = new System.Drawing.Point(10, 19);
            this.txtTransactNum.Name = "txtTransactNum";
            this.txtTransactNum.Size = new System.Drawing.Size(88, 20);
            this.txtTransactNum.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTransactNum);
            this.groupBox2.Location = new System.Drawing.Point(12, 76);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(104, 55);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Transact #";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dtmDateSelect);
            this.groupBox3.Location = new System.Drawing.Point(122, 76);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(213, 55);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Date";
            // 
            // dtmDateSelect
            // 
            this.dtmDateSelect.Location = new System.Drawing.Point(6, 19);
            this.dtmDateSelect.Name = "dtmDateSelect";
            this.dtmDateSelect.Size = new System.Drawing.Size(200, 20);
            this.dtmDateSelect.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtSerialNumber);
            this.groupBox4.Location = new System.Drawing.Point(341, 76);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(93, 55);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Serial Number";
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.Location = new System.Drawing.Point(6, 19);
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(81, 20);
            this.txtSerialNumber.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtToolPurchased);
            this.groupBox5.Location = new System.Drawing.Point(440, 76);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(220, 55);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tool Purchased";
            // 
            // txtToolPurchased
            // 
            this.txtToolPurchased.Location = new System.Drawing.Point(10, 19);
            this.txtToolPurchased.Name = "txtToolPurchased";
            this.txtToolPurchased.Size = new System.Drawing.Size(204, 20);
            this.txtToolPurchased.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtQuantity);
            this.groupBox6.Location = new System.Drawing.Point(757, 76);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(71, 55);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Qty";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(10, 19);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(51, 20);
            this.txtQuantity.TabIndex = 0;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtPrice);
            this.groupBox7.Location = new System.Drawing.Point(666, 76);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(85, 55);
            this.groupBox7.TabIndex = 6;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Price";
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(10, 19);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(69, 20);
            this.txtPrice.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.txtAmount);
            this.groupBox8.Location = new System.Drawing.Point(834, 76);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(116, 55);
            this.groupBox8.TabIndex = 7;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Amount";
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(10, 19);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(100, 20);
            this.txtAmount.TabIndex = 0;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.txtDataDisplay);
            this.groupBox9.Location = new System.Drawing.Point(12, 198);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(938, 324);
            this.groupBox9.TabIndex = 8;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Data Display:";
            // 
            // txtDataDisplay
            // 
            this.txtDataDisplay.BackColor = System.Drawing.SystemColors.Info;
            this.txtDataDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDataDisplay.Location = new System.Drawing.Point(10, 19);
            this.txtDataDisplay.Name = "txtDataDisplay";
            this.txtDataDisplay.Size = new System.Drawing.Size(922, 299);
            this.txtDataDisplay.TabIndex = 0;
            this.txtDataDisplay.Text = "";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.rbOpenFile);
            this.groupBox10.Controls.Add(this.rbCreateFile);
            this.groupBox10.Location = new System.Drawing.Point(347, 12);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(192, 55);
            this.groupBox10.TabIndex = 9;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "File:";
            // 
            // rbOpenFile
            // 
            this.rbOpenFile.AutoSize = true;
            this.rbOpenFile.Location = new System.Drawing.Point(99, 20);
            this.rbOpenFile.Name = "rbOpenFile";
            this.rbOpenFile.Size = new System.Drawing.Size(90, 17);
            this.rbOpenFile.TabIndex = 1;
            this.rbOpenFile.Text = "Open Existing";
            this.rbOpenFile.UseVisualStyleBackColor = true;
            // 
            // rbCreateFile
            // 
            this.rbCreateFile.AutoSize = true;
            this.rbCreateFile.Checked = true;
            this.rbCreateFile.Location = new System.Drawing.Point(7, 20);
            this.rbCreateFile.Name = "rbCreateFile";
            this.rbCreateFile.Size = new System.Drawing.Size(81, 17);
            this.rbCreateFile.TabIndex = 0;
            this.rbCreateFile.TabStop = true;
            this.rbCreateFile.Text = "Create New";
            this.rbCreateFile.UseVisualStyleBackColor = true;
            // 
            // btnCreateFile
            // 
            this.btnCreateFile.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnCreateFile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCreateFile.Location = new System.Drawing.Point(545, 25);
            this.btnCreateFile.Name = "btnCreateFile";
            this.btnCreateFile.Size = new System.Drawing.Size(109, 32);
            this.btnCreateFile.TabIndex = 10;
            this.btnCreateFile.Text = "Create / Open File";
            this.btnCreateFile.UseVisualStyleBackColor = false;
            this.btnCreateFile.Click += new System.EventHandler(this.btnCreateFile_Click);
            // 
            // btnRecordWrite
            // 
            this.btnRecordWrite.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnRecordWrite.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRecordWrite.Location = new System.Drawing.Point(12, 146);
            this.btnRecordWrite.Name = "btnRecordWrite";
            this.btnRecordWrite.Size = new System.Drawing.Size(98, 32);
            this.btnRecordWrite.TabIndex = 11;
            this.btnRecordWrite.Text = "Write a Record";
            this.btnRecordWrite.UseVisualStyleBackColor = false;
            this.btnRecordWrite.Click += new System.EventHandler(this.btnRecordWrite_Click);
            // 
            // btnRecordDelete
            // 
            this.btnRecordDelete.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnRecordDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRecordDelete.Location = new System.Drawing.Point(116, 146);
            this.btnRecordDelete.Name = "btnRecordDelete";
            this.btnRecordDelete.Size = new System.Drawing.Size(169, 32);
            this.btnRecordDelete.TabIndex = 12;
            this.btnRecordDelete.Text = "Delete a Record by Transact #";
            this.btnRecordDelete.UseVisualStyleBackColor = false;
            this.btnRecordDelete.Click += new System.EventHandler(this.btnRecordDelete_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.txtTransactDelete);
            this.groupBox11.Location = new System.Drawing.Point(291, 137);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(104, 55);
            this.groupBox11.TabIndex = 13;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Transact #";
            // 
            // txtTransactDelete
            // 
            this.txtTransactDelete.Location = new System.Drawing.Point(10, 21);
            this.txtTransactDelete.Name = "txtTransactDelete";
            this.txtTransactDelete.Size = new System.Drawing.Size(88, 20);
            this.txtTransactDelete.TabIndex = 0;
            // 
            // btnDisplayData
            // 
            this.btnDisplayData.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnDisplayData.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDisplayData.Location = new System.Drawing.Point(12, 528);
            this.btnDisplayData.Name = "btnDisplayData";
            this.btnDisplayData.Size = new System.Drawing.Size(98, 32);
            this.btnDisplayData.TabIndex = 14;
            this.btnDisplayData.Text = "Display Data";
            this.btnDisplayData.UseVisualStyleBackColor = false;
            this.btnDisplayData.Click += new System.EventHandler(this.btnDisplayData_Click);
            // 
            // btnCloseFile
            // 
            this.btnCloseFile.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnCloseFile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCloseFile.Location = new System.Drawing.Point(116, 528);
            this.btnCloseFile.Name = "btnCloseFile";
            this.btnCloseFile.Size = new System.Drawing.Size(72, 32);
            this.btnCloseFile.TabIndex = 15;
            this.btnCloseFile.Text = "Close File";
            this.btnCloseFile.UseVisualStyleBackColor = false;
            this.btnCloseFile.Click += new System.EventHandler(this.btnCloseFile_Click);
            // 
            // btnDeleteFile
            // 
            this.btnDeleteFile.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnDeleteFile.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDeleteFile.Location = new System.Drawing.Point(194, 528);
            this.btnDeleteFile.Name = "btnDeleteFile";
            this.btnDeleteFile.Size = new System.Drawing.Size(71, 32);
            this.btnDeleteFile.TabIndex = 16;
            this.btnDeleteFile.Text = "Delete File";
            this.btnDeleteFile.UseVisualStyleBackColor = false;
            this.btnDeleteFile.Click += new System.EventHandler(this.btnDeleteFile_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.txtMessageDisplay);
            this.groupBox12.Location = new System.Drawing.Point(13, 567);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(931, 124);
            this.groupBox12.TabIndex = 17;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Message Display";
            // 
            // txtMessageDisplay
            // 
            this.txtMessageDisplay.Location = new System.Drawing.Point(7, 20);
            this.txtMessageDisplay.Name = "txtMessageDisplay";
            this.txtMessageDisplay.Size = new System.Drawing.Size(918, 98);
            this.txtMessageDisplay.TabIndex = 0;
            this.txtMessageDisplay.Text = "";
            // 
            // Abc_Manufacturing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 699);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.btnDeleteFile);
            this.Controls.Add(this.btnCloseFile);
            this.Controls.Add(this.btnDisplayData);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.btnRecordDelete);
            this.Controls.Add(this.btnRecordWrite);
            this.Controls.Add(this.btnCreateFile);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Abc_Manufacturing";
            this.Text = "Abc_Manufacturing";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.TextBox txtTransactNum;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dtmDateSelect;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtSerialNumber;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtToolPurchased;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RichTextBox txtDataDisplay;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton rbOpenFile;
        private System.Windows.Forms.RadioButton rbCreateFile;
        private System.Windows.Forms.Button btnCreateFile;
        private System.Windows.Forms.Button btnRecordWrite;
        private System.Windows.Forms.Button btnRecordDelete;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox txtTransactDelete;
        private System.Windows.Forms.Button btnDisplayData;
        private System.Windows.Forms.Button btnCloseFile;
        private System.Windows.Forms.Button btnDeleteFile;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RichTextBox txtMessageDisplay;
    }
}