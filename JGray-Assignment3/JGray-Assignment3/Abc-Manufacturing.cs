﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JGray_Assignment3
{
    public partial class Abc_Manufacturing : Form
    {
        public Abc_Manufacturing()
        {
            InitializeComponent();
        }

        private void btnCreateFile_Click(object sender, EventArgs e)
        {
            //This method creates the file, or checks to see if the file name is already in use
            FileManager doIt = new FileManager();
            doIt.FileName = txtFileName.Text;
            doIt.CreateFile = rbCreateFile.Checked;
            bool fileFound = doIt.FileFinder(false);
            txtMessageDisplay.Text = doIt.Message;
        }

        private void btnRecordWrite_Click(object sender, EventArgs e)
        {
            FileManager doIt = new FileManager();
            doIt.CreateFile = rbCreateFile.Checked;
            doIt.FileName = txtFileName.Text;
            //The Program first checks to make sure the file in question exists or can be made
            if (doIt.FileFinder(true)||doIt.Message == $"New File created at '{doIt.FileName}' \n")
            {
                doIt.TransactNum = (txtTransactNum.Text + "").Trim();
                doIt.PurchaseDate = dtmDateSelect.ToString().Substring(44,10).Trim() + "";
                doIt.SerialNum = (txtSerialNumber.Text + "").Trim();
                doIt.ToolPurchased = (txtToolPurchased.Text + "").Trim();
                doIt.Price = (txtPrice.Text + "").Trim();
                doIt.Qty = (txtQuantity.Text + "").Trim();
                doIt.Amount = (txtAmount.Text + "").Trim();
                //The program them checks to make sure all inputs are valid
                if (doIt.ValidateInput())
                {
                    //The program then finally writes to the file
                    doIt.WriteToFile(doIt.FileFinder(true));
                }
                txtMessageDisplay.Text = doIt.Message;
            }
            else
            {
                txtMessageDisplay.Text = doIt.Message;
            }
        }

        private void btnRecordDelete_Click(object sender, EventArgs e)
        {
            FileManager doIt = new FileManager();
            doIt.FileName = txtFileName.Text;
            doIt.TransactNum = (txtTransactDelete.Text + "").Trim();
            doIt.DeleteRecord();
            txtMessageDisplay.Text = doIt.Message;
        }

        private void btnDisplayData_Click(object sender, EventArgs e)
        {
            FileManager doIt = new FileManager();
            doIt.FileName = txtFileName.Text;
            doIt.DataDisplay();
            txtMessageDisplay.Text = doIt.Message;
            txtDataDisplay.Text = doIt.Data;
        }

        private void btnCloseFile_Click(object sender, EventArgs e)
        {
            txtDataDisplay.Text =  "";
            txtFileName.Text = "";
            txtMessageDisplay.Text = "File Closed";
        }

        private void btnDeleteFile_Click(object sender, EventArgs e)
        {
            FileManager doIt = new FileManager();//Don't delete important files thank you
            doIt.FileName = txtFileName.Text;
            doIt.DeleteFile();
            txtMessageDisplay.Text = doIt.Message;
        }
    }
}
