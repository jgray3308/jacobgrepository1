﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JGray_Assignment3
{
    class FileManager
    {
        public string FileName { get; set; }
        public Boolean CreateFile { get; set; }
        public string TransactNum { get; set; }
        public string PurchaseDate { get; set; }
        public string SerialNum { get; set; }
        public string ToolPurchased { get; set; }
        public string Price { get; set; }
        public string Qty { get; set; }
        public string Amount { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }

        StreamWriter writer;
        StreamReader reader;
        string record = "";
        /// <summary>
        /// Searches out to see if a file exists, and makes one only if
        /// the user is creating a file, and in no other context.
        /// Returns a boolean confirming the existance of said file.
        /// </summary>
        /// <param name="writing"></param>
        /// <returns></returns>
        public bool FileFinder(bool writing)
        {
            if (CreateFile)//Runs this half if the user has checked the create file box
            {
                if (File.Exists(FileName))
                {
                    Message = $"Existing File Found at '{FileName}' \n";
                    return true;
                }  
                else
                {
                    Message = $"New File created at '{FileName}' \n";
                    if (!writing)//An additional check to opt out of making the file if the user is writing to it instead
                    {
                        try
                        {
                            writer = new StreamWriter(FileName, append: false);
                        }
                        catch (Exception ex)
                        {
                            Message = $"Error creating file: {ex.Message}";
                        }
                        finally
                        {
                            if (writer != null)
                            {
                                writer.Close();
                            }
                        }
                    }
                    return false;
                } 
            }
            else//Runs this half if the user has checked the Open File Box
            {
                if (File.Exists(FileName))
                {
                    Message = $"File Found and Opened at '{FileName}' \n";
                    return true;
                }
                else
                {
                    Message = $"File Not Found at '{FileName}' \n";
                    return false;
                }
            }
        }

        /// <summary>
        /// Writes to the file in question, uses the existingFile boolean
        /// to return an error statement to the message box
        /// </summary>
        /// <param name="existingFile"></param>
        public void WriteToFile(bool existingFile)
        {
            if (existingFile)//If the file does not exist it will not write to it.
            {
                try
                {
                    writer = new StreamWriter(FileName, append: true);//Writes to file using append true always
                    record = $"{TransactNum}::{PurchaseDate}::{SerialNum}::{ToolPurchased}::{Price}::{Qty}::{Amount}";

                    writer.WriteLine(record);
                    Message = $"Record added: {FileName}";
                }
                catch (Exception ex)
                {
                    Message = ($"Exception adding new record: {ex.Message}");
                }
                finally
                {
                    if (writer != null)
                    {
                        writer.Close();
                    }
                }

            }
            else
            {
                Message = $"File has not yet been created at {FileName} to be written to.";
            }

        }

        /// <summary>
        /// Displays all of the information within the file in the 
        /// Data string to be sent back to the host class
        /// </summary>
        public void DataDisplay()
        {
            try
            {
                reader = new StreamReader(FileName);
            }
            catch (Exception ex)
            {
                Message = $"Exception opening a reader: {ex.Message}";
                return;
            }
            record = "";
            Data = $"{"#".PadRight(5)}\t{"Purchase-Date".PadRight(16)}\t{"Serial#".PadRight(5)}\t{"Manufacturing Tools".PadRight(26)}\t{"Price".PadRight(8)}\t{"Qty".PadRight(5)}\t{"Amount"}\n __________________________________________________________________________________________\n";

            try
            {
                int count = 0;
                while (!reader.EndOfStream)
                {
                    record = reader.ReadLine();
                    string[] fields = record.Split(new string[] { "::" }, StringSplitOptions.None);
                    Data += $"{fields[0].PadRight(5)}\t{fields[1].PadRight(16)}\t{fields[2].PadRight(10)}\t{fields[3].PadRight(28)}\t{fields[4].PadRight(8)}\t{fields[5].PadRight(5)}\t{fields[6]}\n";
                    count = count + 1;
                    Message = $"Records Displayed: {count}";
                }
            }
            catch (Exception ex)
            {
                Message = $"Exception using a reader: {ex.Message} ";
                return;
            }
            finally
            {
                Data += "__________________________________________________________________________________________";
                if (reader != null)
                {
                    reader.Close();
                }
            }
        }

        /// <summary>
        /// Removes a line of data from the file based on it's 
        /// Transact Number. 
        /// </summary>
        public void DeleteRecord()
        {
            try
            {
                reader = new StreamReader(FileName);
            }
            catch (Exception ex)
            {
                Message = $"Exception opening a reader: {ex.Message}";
                return;
            }
            record = "";
            int deleteIndex = -1;
            int fileLength = 0;
            while (!reader.EndOfStream)//First, it reads throgh the file, collecting data on how many records there are, and which one matches the delete number
            {
                try
                {
                    record = reader.ReadLine();
                    string firstNum = record.Substring(0, record.IndexOf("::"));
                    if (firstNum.Equals(TransactNum))//In the event of multiple inputs with the same transact number, it will delete the last index of said number
                    {
                        deleteIndex = fileLength;
                    }
                    fileLength = fileLength + 1;
                }
                catch (Exception ex)
                {
                    Message = $"Exception using a reader: {ex.Message}";
                    return;
                }
            }
            reader.Close();
            if(deleteIndex != -1)//From here onward, the program will only continue if there is something to delete that matches the delete number
            {
                try
                {
                    reader = new StreamReader(FileName);
                }
                catch (Exception ex)
                {
                    Message = $"Exception opening a reader: {ex.Message}";
                    return;
                }
                record = "";
                string[] recordStorage = new string[fileLength];
                int placeTracker = 0;
                while (!reader.EndOfStream)//The program then reads through the file again, storing each record in an array
                {
                    try
                    {
                        record = reader.ReadLine();
                        recordStorage[placeTracker] = record;
                        placeTracker = placeTracker + 1;
                    }
                    catch (Exception ex)
                    {
                        Message = $"Exception using a reader: {ex.Message}";
                        return;
                    }
                }
                reader.Close();
                try
                {
                    //Then the program overwrites the file, and
                    //re adds every record back to the file except the one that is getting deleted
                    writer = new StreamWriter(FileName, append: false);
                    for(int i = 0; i < recordStorage.Length; i++)
                    {
                        if (i != deleteIndex)
                        {
                            writer.WriteLine(recordStorage[i]);
                        }
                    }
                    Message = "File Successfully Removed";
                }
                catch (Exception ex)
                {
                    Message = ($"Exception removing record: {ex.Message}");
                }
                finally
                {
                    if (writer != null)
                    {
                        writer.Close();
                    }
                }
            }
            else
            {
                Message = $"File with a number of {TransactNum} not found";
            }
            
        }

        /// <summary>
        /// Completely deletes the chosen file from the computer
        /// But only if it exists and would not cause any errors
        /// </summary>
        public void DeleteFile()
        {
            try
            {
                if (File.Exists(FileName))
                {
                    File.Delete(FileName);
                    Message = $"File at {FileName} deleted";
                }
                else
                {
                    Message = $"File at {FileName} not found";
                }
            }
            catch (Exception ex)
            {
                Message = Message = $"Error Deleting File: {ex.Message}";
            }
        }

        /// <summary>
        /// Used to validate the incoming information that would be 
        /// put into the data file
        /// </summary>
        /// <returns></returns>
        public bool ValidateInput()
        {
            Message = "";
            bool errorFree = true;

            Regex transact = new Regex(@"^\d+$");
            Regex serial = new Regex(@"^\d{4}$");
            Regex pricey = new Regex(@"^\$\d*.?\d\d?$");
            if (TransactNum == "")
            {
                Message += "Transact Number input is required \n";
                errorFree = false;
            }
            else if(!transact.IsMatch(TransactNum))
            {
                Message += "Transact Number must be numeric\n";
                errorFree = false;
            }
            else if(TransactNum.Length > 5){
                Message += "Transact Number must be less than 5 digits in length \n";
                errorFree = false;
            }
            if (SerialNum == "")
            {
                Message += "Serial Number input is required \n";
                errorFree = false;
            }
            else if (!serial.IsMatch(SerialNum))
            {
                Message += "Serial Number must be a 4 digit code \n";
                errorFree = false;
            }
            if (ToolPurchased == "")
            {
                Message += "Tool Name input is required \n";
                errorFree = false;
            }
            else if (TransactNum.Length > 25)
            {
                Message += "Tool Name must be less than 25 characters in length \n";
                errorFree = false;
            }
            if (Price == "")
            {
                Message += "A price input is required \n";
                errorFree = false;
            }
            else if (!pricey.IsMatch(Price))
            {
                Message += "Price input must be numeric and have a leading $ \n";
                errorFree = false;
            }
            if (Qty == "")
            {
                Message += "A Quantity input is required \n";
                errorFree = false;
            }
            else if (!transact.IsMatch(Qty))
            {
                Message += "Quantity value must a positive integer\n";
                errorFree = false;
            }
            if (Amount == "")
            {
                Message += "An Amount input is required \n";
                errorFree = false;
            }
            else if (!pricey.IsMatch(Amount))
            {
                Message += "Amount input must be numeric and have a leading $ \n";
                errorFree = false;
            }
            return errorFree;
        }
    }
}
